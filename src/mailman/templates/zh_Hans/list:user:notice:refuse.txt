您对$listname邮件列表的请求

    $request

已被列表管理人拒绝。  版主给出了
以下是拒绝你的请求的原因。

"$reason"

任何问题或意见都应该直接提交给列表管理员
at:

    $owner_email
